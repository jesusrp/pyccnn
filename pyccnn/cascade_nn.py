__author__ = 'jesusrp'

import numpy as np


def xavier_init_fn(n_in_connections):

    return np.random.randn()*(1/np.sqrt(n_in_connections))

def sigmoid(y):

    f = 1 / (1.0 + np.exp(-y))

    return f

def d_sigmoid(y):

    f = 1 / (1.0 + np.exp(-y))
    df = f * (1.0 - f)

    return df

def ReLU(y):

    return y * (y > 0)

def d_ReLU(y):

    return 1. * (y > 0)

def softmax(y):

    exps = np.exp(y - np.max(y))

    return exps / np.sum(exps)

def d_softmax(y):

    N = len(y)
    m = np.max(y)
    df = np.array([np.exp(y[i] - m)*np.sum([y[j] - m for j in list(set(range(N))-set([i]))]) for i in range(N)])/sum(np.exp(y - np.max(y)))

    return df

class CandidateNode(object):
    """
        A candidate for the cascade-correlation algorithm.
    """
    
    def __init__(self, connection_weights):
        self.weights = connection_weights
        self.score = 0.0
        self.activations = []


class CascadeNN(object):
    """
        A Cascade Neural Network.
    """

    """ Initialize parameters of the network given a number of  """
    """ candidate nodes and a weight initialization function    """
    def __init__(self,
                 n_input_nodes,
                 n_output_nodes,
                 activation_fn,
                 d_activation_fn,
                 output_activation_fn,
                 d_output_activation_fn,
                 loss_fn,
                 d_loss_output_fn,
                 weight_init_fn=xavier_init_fn):

        self.n_input_nodes = n_input_nodes

        self.n_output_nodes = n_output_nodes

        # Use a bias unit
        self.n_non_output_nodes = n_input_nodes + 1

        self.weight_init_fn = weight_init_fn

        # Initialise weights. Connect each input node to all output nodes (use a bias unit)
        weights = [[self.weight_init_fn(self.n_non_output_nodes) for _ in range(self.n_non_output_nodes)] for _ in range(self.n_output_nodes)]
        self.output_connections = np.array(weights)

        # Initially the NN consists of no hidden units
        self.cascade_connections = []
        self.n_cascade_connections = 0

        self.activation_fn = activation_fn
        self.d_activation_fn = d_activation_fn
        self.output_activation_fn = output_activation_fn
        self.d_output_activation_fn = d_output_activation_fn

        self.activations = np.append(np.zeros(self.n_input_nodes+1))

        self.loss_fn = loss_fn
        self.d_loss_fn = d_loss_output_fn

        self.weights = self.output_connections

		# For later use (not used at the moment)
        self._last_weight_change = np.ones((n_output_nodes, n_input_nodes + 1))
        self._last_weight_derivative = np.ones((n_output_nodes, n_input_nodes + 1))

    @staticmethod
    def sum_squared_error(result, target):

        return np.sum((result - target)**2)/2

    @staticmethod
    def d_squared_error(result, target, d_output_activation_fn):

        return (result - target)*d_output_activation_fn(result)

    @staticmethod
    def cross_entropy(result, target):

        return -np.sum([target[i]*np.log(result[i])+(1-target[i])*np.log(1-result[i]) for i in range(len(result))])

    @staticmethod
    def d_softmax_cross_entropy(result, target, d_output_activation_fn):

        return result - target

    def add_hidden_node(self, candidate_weights):

        self.cascade_connections.append(candidate_weights)
        new_output_connections = np.array([[self.weight_init_fn(1) for _ in range(self.n_output_nodes)]])
        self.output_connections = np.concatenate((self.output_connections, new_output_connections.T), axis=1)

        self.n_cascade_connections += 1
        self.n_non_output_nodes += 1

        self.activations = np.append(np.array([1.0]), np.zeros(self.n_non_output_nodes + self.n_output_nodes))
        self.weights = self.output_connections

    """ Compute the correlations between the candidate unit's outputs and the   """
    """ residual error we are trying to eliminate                               """
    def compute_correlations(self, candidate_activation, errors, mean_errors):

        sums = np.zeros(len(errors[0]))
        for o in range(len(errors[0])):
            for p in range(len(candidate_activation)):
                x = candidate_activation[p] * (errors[p][o] - mean_errors[o])
                sums[o] += x

        return sums

    def back_propagation(self, output, target):

        derivatives = [np.array([0.0 for _ in range(self.n_non_output_nodes)]) for _ in range(self.n_output_nodes)]
        d_loss_output = self.d_loss_fn(output,target,self.d_output_activation_fn)

        for non_output_node_index in range(self.n_non_output_nodes):
            for output_node_index in range(self.n_output_nodes):
                dwde = self.activations[non_output_node_index] * d_loss_output[output_node_index]
                derivatives[output_node_index][non_output_node_index] += dwde

        return derivatives

    def gradient_descent_step(self, inputs, targets, learning_rate):

        hits = 0
        loss = 0.0
        derivatives = np.zeros((self.n_output_nodes, self.n_non_output_nodes))
        for i, input in enumerate(inputs):
            output = self.feed_forward(input)
            derivatives = np.add(derivatives, self.back_propagation(output, targets[i]))
            output_activation = self.output_activation_fn(output)
            loss += self.loss_fn(output_activation, targets[i])
            if np.argmax(output_activation) == np.argmax(targets[i]):
                hits += 1

        self.weights = self.weights - np.multiply(derivatives, learning_rate)                             # weight update via gradient descent
        self.output_connections = self.weights

        return loss, hits

    """ Update the weights of the connections ending at the output of the temporarily frozen net by applying gradient"""
    """ descent with backprop until convergence is reached (equivalently, until the error no longer decreases) """
    def train_temp_net(     self,
                            inputs,
                            targets,
                            mini_batch_size,               # mini batch = list of tuples (input, target)
                            learning_rate,                 # learning rate
                            max_epochs):                   # one epoch = one forward-backward pass of all inputs

        self.inputs = inputs
        self.targets = targets

        n_inputs = len(inputs)
        loss_difference = np.infty
        epsilon = 1e-5
        losses = np.zeros(max_epochs)
        epoch = 1

        while ((loss_difference > epsilon) & (epoch <= max_epochs)):
            loss = 0.0
            hits = 0
            offset = 0
            batch_size = np.floor_divide(n_inputs,mini_batch_size)*mini_batch_size
            for offset in range(0, batch_size, mini_batch_size):

                [batch_loss, batch_hits] = self.gradient_descent_step(self.inputs[offset:offset + mini_batch_size], targets[offset:offset + mini_batch_size], learning_rate)
                loss += batch_loss
                hits += batch_hits

            [batch_loss, batch_hits] = self.gradient_descent_step(self.inputs[offset:offset + (n_inputs - batch_size)], targets[offset:offset + (n_inputs - batch_size)], learning_rate)
            loss += batch_loss
            hits += batch_hits

            loss = loss/n_inputs

            if epoch > 1:
                loss_difference = prev_loss - loss

            print("epoch: "+repr(epoch)+" loss: "+repr(loss)+" accuracy: "+repr(hits/n_inputs)+" diff :"+repr(loss_difference))

            prev_loss = loss

            losses[epoch-1] = loss
            epoch += 1

        return losses

    "Learn the structure and weights of a NN from given labelled samples and learning parameters"
    def train_cascade_net(  self,
                            inputs,                                 # training inputs
                            targets,                                # training targets
                            max_hidden_nodes,                       # number of hidden nodes allowed in the final net
                            n_candidate_nodes_per_pool,             # number of candidate nodes to generate in each iteration of the CC algorithm
                            train_temp_net_max_epochs,
                            train_candidates_max_epochs,
                            learning_rate,                          # learning_rate
                            mini_batch_size):                       # mini batch = list of tuples (train_input, train_target)

        print('Training temporary cascade net ('+str(self.n_input_nodes)+'='+str(self.n_output_nodes)+')...\n')
        self.train_temp_net(inputs, targets, mini_batch_size, learning_rate, train_temp_net_max_epochs)

        hidden_layer_sizes = []
        deepest_hidden_layer_size = 0
        prev_losses = [np.infty]
        while self.n_cascade_connections < max_hidden_nodes: # accuracy threshold
            print('\nGenerating hidden unit ' + repr(self.n_cascade_connections+1) + ' of ' + repr(max_hidden_nodes) + ' among ' + repr(n_candidate_nodes_per_pool*2) + ' candidates...\n')
            [winning_candidate, new_hidden_layer] = self.generate_cascade_unit(learning_rate,n_candidate_nodes_per_pool,train_candidates_max_epochs,deepest_hidden_layer_size)
            self.add_hidden_node(winning_candidate.weights)
            if new_hidden_layer:
                hidden_layer_sizes.append(1)
                deepest_hidden_layer_size = 1
            else:
                hidden_layer_sizes[len(hidden_layer_sizes)-1] += 1
                deepest_hidden_layer_size += 1

            if self.n_cascade_connections < max_hidden_nodes:
                print('Training temporary cascade net ('+str(self.n_input_nodes)+'='+'='.join([str(hidden_layer_size) for hidden_layer_size in hidden_layer_sizes])+'='+str(self.n_output_nodes)+')...\n')
            else:
                print('Training final cascade net ('+str(self.n_input_nodes)+'='+'='.join([str(hidden_layer_size) for hidden_layer_size in hidden_layer_sizes])+'='+str(self.n_output_nodes)+')...\n')
            losses = self.train_temp_net(inputs, targets, mini_batch_size, learning_rate, train_temp_net_max_epochs)

            if losses[len(losses)-1] > prev_losses[len(prev_losses)-1]:
                break

        return [losses, hidden_layer_sizes]

    """ Feed input values forward to the hidden nodes """
    def feed_forward_hidden_nodes(self, input):

        for i, value in enumerate(input):
            self.activations[i + 1] = value

        for i, hidden_weights in enumerate(self.cascade_connections):
            output = np.inner(hidden_weights, self.activations[:len(hidden_weights)])
            self.activations[i + self.n_input_nodes + 1] = self.activation_fn(output)

    """ Feed hidden values forward to the output nodes """
    def feed_forward_output(self):

        output = [np.inner(output_weights, self.activations[:len(output_weights)]) for output_weights in self.output_connections]

        return output

    def feed_forward(self, input):

        self.feed_forward_hidden_nodes(input)

        return self.feed_forward_output()

    """ Compute correlation derivative """
    def compute_correlation_derivative( self,
                                        candidate_weights,
                                        errors,
                                        mean_errors,
                                        candidate_activations,
                                        sign_correlation_with_outputs):

        derivatives = np.zeros(len(candidate_weights))
        for i in range(len(candidate_weights)):
            dcdw = 0.0
            for p in range(len(errors)):
                theta0 = 0.0
                for o in range(len(mean_errors)):
                    theta0 += sign_correlation_with_outputs[o] * (errors[p][o] - mean_errors[o]) * self.d_activation_fn(candidate_activations[p]) * candidate_weights[i]
                    dcdw += theta0
            derivatives[i] = dcdw

        return derivatives

    """ Select the optimal unit out of a pool of candidates according to the cascade correlation algorithm"""
    def generate_cascade_unit(self,
                              learning_rate,
                              n_candidate_nodes,              # Number of candidates for each hidden unit generation
                              max_epochs,
                              deepest_hidden_layer_size):

        errors = [[]*self.n_output_nodes]*len(self.inputs)
        for i, input in enumerate(self.inputs):
            output_activation = self.output_activation_fn(self.feed_forward(input))
            error = output_activation-self.targets[i]
            errors[i] = error

        mean_errors = [sum([x[y] for x in errors]) / len(self.inputs) for y in range(self.n_output_nodes)]

        # Generate a pool of candidate units

        #TODO Design a different heuristic for generating candidates?

        descendant_candidates = [CandidateNode(np.array([self.weight_init_fn(self.n_non_output_nodes) for _ in range(self.n_non_output_nodes)])) for _ in range(n_candidate_nodes)]
        sibling_candidates = [CandidateNode(np.array([self.weight_init_fn(self.n_non_output_nodes-deepest_hidden_layer_size) for _ in range(self.n_non_output_nodes-deepest_hidden_layer_size)])) for _ in range(n_candidate_nodes)]

        best_descendant_candidate = self.generate_best_candidate_by_pool(learning_rate, max_epochs, errors, mean_errors, descendant_candidates)
        if deepest_hidden_layer_size == 0:
            new_hidden_layer = True
            best_candidate = best_descendant_candidate
        else:
            best_sibling_candidate = self.generate_best_candidate_by_pool(learning_rate, max_epochs, errors, mean_errors, sibling_candidates)
            if best_descendant_candidate.score > best_sibling_candidate.score:
                new_hidden_layer = True
                best_candidate = best_descendant_candidate
            else:
                new_hidden_layer = False
                best_candidate = best_sibling_candidate

        return best_candidate, new_hidden_layer


    def generate_best_candidate_by_pool(self, learning_rate, candidates_max_epochs, errors, mean_errors, candidates):

        highest_correlation_score = 0

        epochs = 1
        epochs_without_improvement = 0

        while epochs <= candidates_max_epochs and epochs_without_improvement <= 1:

            last_highest_correlation_score = highest_correlation_score

            for candidate in candidates:
                candidate.activations.clear()

            for input in self.inputs:
                self.feed_forward_hidden_nodes(input)
                for candidate in candidates:
                    candidate.activations.append(self.activation_fn(np.inner(candidate.weights, self.activations[:len(candidate.weights)])))

            for candidate in candidates:
                correlations = self.compute_correlations(candidate.activations, errors, mean_errors)
                correlation_signs = [np.sign(correlation) for correlation in correlations]
                candidate.score = sum(correlations)
                derivatives = self.compute_correlation_derivative(candidate.weights,
                                                                  errors,
                                                                  mean_errors,
                                                                  candidate.activations,
                                                                  correlation_signs)

                candidate.weights = candidate.weights + np.multiply(derivatives, learning_rate)      # weight update via gradient ascent of the correlation

			# Check stopping criterion
            best_candidate = max(candidates, key=lambda x: x.score)
            highest_correlation_score = best_candidate.score

            if highest_correlation_score <= last_highest_correlation_score:
                epochs_without_improvement += 1

            epochs += 1

        return best_candidate









