__author__ = 'jesusrp'

import sys
import numpy as np
import math
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from pyccnn import cascade_nn
from matplotlib import pyplot as plt
import csv

""" Evaluates the CC algorithm (tests its performance) on a NN on a dataset provided in the /datasets folder """

def main():

    filename = sys.argv[1]
    path_to_dataset = 'datasets/' + filename

    subset_size_perc = 0.05

    inputs = []
    n_inputs = 0
    targets = []
    with open(path_to_dataset) as f:
        for line in f:
            if line[-1] == '\n':
                line = line[:-1]
            parts = line.split(',')
            n_features = len(parts) - 1
            inputs.append([float(parts[i]) for i in range(n_features)])
            targets.append(parts[n_features])
            n_inputs += 1

    inputs = inputs[0:int(subset_size_perc*n_inputs)]
    targets = targets[0:int(subset_size_perc*n_inputs)]

    label_encoder = LabelEncoder()
    integer_encoded = label_encoder.fit_transform(np.array(targets))

    n_labels = max(integer_encoded) - min(integer_encoded) + 1

    np.random.seed(0)          # use a constant seed of random number generation for reproducible results

    net = cascade_nn.CascadeNN(n_features,
                               n_labels,
                               activation_fn=cascade_nn.sigmoid,
                               d_activation_fn=cascade_nn.d_sigmoid,
                               output_activation_fn=cascade_nn.softmax,
                               d_output_activation_fn=cascade_nn.d_softmax,
                               loss_fn=cascade_nn.CascadeNN.cross_entropy,
                               d_loss_output_fn=cascade_nn.CascadeNN.d_softmax_cross_entropy,
                               weight_init_fn=cascade_nn.xavier_init_fn)

    split_element = math.floor(len(inputs)*0.7)        # 70% training set // 30% test set

    train_inputs = inputs[:split_element]
    test_inputs = inputs[split_element+1:]

    onehot_encoder = OneHotEncoder(sparse=False)
    integer_encoded = integer_encoded.reshape(len(integer_encoded), 1)
    enc_targets = onehot_encoder.fit_transform(integer_encoded)

    train_targets = targets[:split_element]
    enc_train_targets = enc_targets[:split_element]
    test_targets = targets[split_element+1:]
    enc_test_targets = enc_targets[split_element+1:]

    [losses_per_epoch, hidden_layer_sizes] = net.train_cascade_net( train_inputs,
                                                                enc_train_targets,
                                                                max_hidden_nodes=5,
                                                                n_candidate_nodes_per_pool=5,
                                                                train_temp_net_max_epochs=1000,
                                                                train_candidates_max_epochs=20,
                                                                learning_rate=1e-5,
                                                                mini_batch_size=int(2**6))        # pick a size of a power of 2 to speed up computation

    plt.plot(range(1,len(losses_per_epoch)+1), np.array(losses_per_epoch)/len(train_inputs))
    plt.xticks(range(1,len(losses_per_epoch)+1))
    plt.xlabel('iterations')
    plt.ylabel('loss')
    plt.title(str(net.n_input_nodes)+'='+'='.join([str(hidden_layer_size) for hidden_layer_size in hidden_layer_sizes])+'='+str(net.n_output_nodes)+' CCNN loss')
    plt.savefig('figures/loss vs iterations.png')
    plt.close()

    hits = 0
    for i, train_input in enumerate(train_inputs):
        output_activation = cascade_nn.softmax(net.feed_forward(train_input))
        if np.argmax(output_activation) == np.argmax(train_targets[i]):
            hits += 1

    train_accuracy = hits / len(train_inputs)

    hits = 0
    enc_test_targets_pred = []
    for i, test_input in enumerate(test_inputs):
        enc_test_target_pred = np.zeros(n_labels)
        output_activation = cascade_nn.softmax(net.feed_forward(test_input))
        if np.argmax(output_activation) == np.argmax(test_targets[i]):
            hits += 1
        enc_test_target_pred[np.argmax(output_activation)] = 1
        enc_test_targets_pred.append(enc_test_target_pred)

    test_accuracy = hits / len(test_inputs)

    print('\ntrain accuracy: '+repr(train_accuracy*100)+' %  test accuracy: '+repr(test_accuracy*100)+' %\n')

    train_dataset = [train_input.__add__([train_targets[i]]) for i,train_input in enumerate(train_inputs)]

    with open('datasets/'+filename+'_train',"w") as f:
        wr = csv.writer(f)
        wr.writerows(train_dataset)

    test_dataset = [test_input.__add__([test_targets[i]]) for i,test_input in enumerate(test_inputs)]

    with open('datasets/'+filename+'_test',"w") as f:
        wr = csv.writer(f)
        wr.writerows(test_dataset)

    target_to_enc_target = dict()
    n_keys = 0

    for i,test_target in enumerate(test_targets):
        if test_target not in target_to_enc_target:
            target_to_enc_target[test_target] = enc_test_targets[i]
            n_keys += 1
        if n_keys == n_labels:
            break

    target_to_enc_target_items = list(target_to_enc_target.items())

    pred_labels = []
    for enc_test_target_pred in enc_test_targets_pred:
        for item in target_to_enc_target_items:
            if np.array_equal(np.array(item[1]), np.array(enc_test_target_pred)):
                pred_labels.append(item[0])
                break

    pred_test_dataset = [test_input.__add__([pred_labels[i]]) for i,test_input in enumerate(test_inputs)]

    with open('datasets/'+filename+'_test_pred',"w") as f:
        wr = csv.writer(f)
        wr.writerows(pred_test_dataset)

    pass


if __name__ == "__main__":
    main()