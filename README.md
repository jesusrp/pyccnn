-----------------------------------------------------------------------------------------------------------------------------
PyCCNN: A Python implementation of (Deep) Cascade Correlation Neural Networks
-----------------------------------------------------------------------------------------------------------------------------

## The Algorithm

Cascade-Correlation (CC) combines two ideas: The first is the cascade architecture, in which hidden units are added
only one at a time and do not change after they have been added. The second is the learning algorithm,
which creates and installs the new hidden units. For each new hidden unit, the algorithm tries to maximize
the magnitude of the correlation between the new unit's output and the residual error signal of the net.

The algorithm is realized in the following way:

1. Start with a minimal network consisting only of an input and an output layer (both layers being *fully connected*, i.e.
each input neuron being linked to every output neuron).

2. Train all the connections ending at an output unit with a usual learning algorithm (gradient descent, quickprop)
until the error of the net no longer decreases.

3. Generate the so-called candidate units (every candidate unit is connected with all input units
and with all existing hidden units and between the pool of candidate units and the output units there are no weights).

4. Try to maximize the magnitude of the correlation between the activation of the candidate units and the residual
error of the net by training all the links leading to a candidate unit with an ordinary learning algorithm
(gradient ascent) until the correlation scores no longer improves.

5. Choose the candidate unit with the maximum correlation, freeze its incoming weights and add it to the net (in order
to change the candidate unit into a hidden unit, generate links between the selected unit and all the output units),
thus obtaining a new permanent feature detector.

6. If the overall error of the net is not minimum then loop back to step 2.



## Usage

- Install dependencies* from PyPi. From the root repo folder:

    pip3 install -r requirements.txt

    *Supports Python 3.5

- Once the repository is cloned, from the *root repo folder* run the script:

    python3.5 evaluate_ccnn.py [name_of_dataset_in_datasetsfolder]

    e.g.

    python3.5 evaluate_ccnn.py letter-recognition.data

    *The script is tested for a generic CSV-format dataset from the UCI ML Repository with *numeric inputs/attributes*
    and *categorical target/labels*, being the target the last element of the comma-separated series.
    The suffix .data denotes the dataset itself while .names refers to a description of the dataset.

- The script will produce in the datasets/ folder the training set, the test set and the dataset with the predicted
targets/labels for the test set. The accuracy for the train and test sets will be printed.

